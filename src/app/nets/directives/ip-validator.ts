import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export function validIp(propertyName:string): ValidatorFn {
    let e = {}
    let ipRe = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
    return (control: AbstractControl): {[key: string]: any} | null => {
        const validIp = ipRe.test(control.value);

        e[propertyName]=validIp;
        console.log(propertyName+':'+e[propertyName] );
        return propertyName ? {propertyName : {value:  e[propertyName]}, 'source': ipRe.source } : null;
    };
}
export function validMask(propertyName:string): ValidatorFn {
    let e = {}
    let maskRe = /^(((255\.){3}(255|254|252|248|240|224|192|128|0+))|((255\.){2}(255|254|252|248|240|224|192|128|0+)\.0)|((255\.)(255|254|252|248|240|224|192|128|0+)(\.0+){2})|((255|254|252|248|240|224|192|128|0+)(\.0+){3}))$/g
    return (control: AbstractControl): {[key: string]: any} | null => {
        const validMask = maskRe.test(control.value);

        e[propertyName]=validMask;
        console.log(propertyName+':'+e[propertyName] );
        return validMask ? {propertyName : {value:validMask}} : null;
    };
}