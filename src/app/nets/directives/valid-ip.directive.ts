import { Directive,Input } from '@angular/core';
import { AbstractControl, ValidatorFn, Validator,NG_VALIDATORS } from '@angular/forms';
import {  } from '@angular/core';
import { validIp } from './ip-validator';

@Directive({
    selector: `[ipValid]`,
    providers: [{provide: NG_VALIDATORS, useExisting: IpValidator, multi: true}]
})
export class IpValidator implements Validator {
    // @Input('appValidIp') validIp: string;

    validate(control: AbstractControl): {[key: string]: any} | null {
        return validIp('validIp')(control);
    }
}


