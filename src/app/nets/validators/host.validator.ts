import {FormControl, ValidatorFn} from '@angular/forms';

export class HostValidator {
    static readonly  hostRe = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$/;
    static validHost(fc: FormControl) {
        if(HostValidator.hostRe.test(fc.value)){
            return ({validHost: true});
        } else {
            return (null);
        }
    }
}