import {FormControl, ValidatorFn,AbstractControl} from '@angular/forms';

export class IpValidator {
    static readonly  ipRe = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/;
    static validIp(fc: FormControl) {
        if(IpValidator.ipRe.test(fc.value)){
            return ({invalidIp: false});
        } else {
            return (null);
        }
    }
}
