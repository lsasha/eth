import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import  { NodeService } from "./services/node.service";
import  { Node,net,ip,dns, } from "./models/node.model";
import { Subscription } from 'rxjs-compat/Subscription';
// import { validIp,validMask } from './directives/mac-validator';

@Component({
    selector: 'app-nets',
    templateUrl: './nets.component.html',
    styleUrls: ['./nets.component.css']
})
export class NetsComponent implements OnInit {

    subscription: Subscription;
    submitted: boolean = false;
    node: Node;

    // ip: IP;

    netForm: FormGroup;
    // ipForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private nodeService: NodeService ) {

    }

    ngOnInit() {
        this.getNode();
        this.initForm();
    }

    getNode() {
        this.subscription = this.nodeService.nodeChanged
            .subscribe(
                (node: Node) => {
                    this.node = node;
                }
            );
        this.node = this.nodeService.getNode();
    }

    onSubmit()    {
        this.submitted = true;

        // stop here if form is invalid
        if (this.netForm.invalid) {
            return;
        }
        this.nodeService.sendData(this.netForm.value);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    get f() { return this.netForm.controls; }

    initForm()  {

        this.netForm = this.fb.group({
            mac: new FormControl({value: this.node.mac, disabled: true},Validators.pattern(/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/)),
            host: new FormControl(this.node.host,Validators.compose([
                // validIp('validIp'),
                Validators.required,
                Validators.pattern(/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/)
            ])),
            ip: new FormControl(this.node.ip,Validators.compose([
                // validIp('validIp'),
                Validators.required,
                Validators.pattern(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)
            ])),
            mask: new FormControl(this.node.mask,Validators.compose([
                // validMask('validMask'),
                Validators.required,
                Validators.pattern(/^(((255\.){3}(255|254|252|248|240|224|192|128|0+))|((255\.){2}(255|254|252|248|240|224|192|128|0+)\.0)|((255\.)(255|254|252|248|240|224|192|128|0+)(\.0+){2})|((255|254|252|248|240|224|192|128|0+)(\.0+){3}))$/)
            ])),
            gateway: new FormControl(this.node.gateway,Validators.compose([
                // validIp('validIp'),
                Validators.required,
                Validators.pattern(/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/)
            ]))
            ,
            server1: new FormControl(this.node.server1,Validators.compose([
                // validIp('validIp'),
                Validators.required,
                Validators.pattern(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)
            ]))
            ,
            server2: new FormControl(this.node.server2,Validators.compose([
                // validIp('validIp'),
                Validators.required,
                Validators.pattern(/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)
            ]))
        });
    }

    onIpDhcpClick() {
        this.nodeService.storeHandy('IP',this.netForm.value);
        this.nodeService.setDhcp('IP');
        this.getNode();
        this.initForm();
    }

    onIpHandyClick() {
        this.nodeService.restoreHandy('IP');
        this.getNode();
        this.initForm();
    }

    onDnsDhcpClick() {
        this.nodeService.storeHandy('DNS',this.netForm.value);
        this.nodeService.setDhcp('DNS');
        this.getNode();
        this.initForm();
    }

    onDnsHandyClick() {
        this.nodeService.restoreHandy('DNS');
        this.getNode();
        this.initForm();
    }
    onCancel() {
        this.nodeService.nodeReset();
        this.initForm();
    }
}
