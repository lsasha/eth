import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs/index';
import { Node,NET,IP,DNS, node, net,ip,dns } from '../models/node.model';
import { Subject } from 'rxjs/Subject';
import { Creator } from '../lib/creator';

@Injectable()
export  class NodeService {
    nodeChanged = new Subject <Node>();

    private node: Node = new Node(node.mac,node.host,node.ip,node.mask,node.gateway,node.server1,node.server2);
    ipHandy: IP = ip;
    dnsHandy: DNS;

    constructor(private creator: Creator) {

    }
    // private ip: IP = new IP(ip.ip,ip.mask);

    getNode(): Node  {
        return this.node;
    }

    setNode(obj: any) {
        this.node=Object.assign(this.node,obj);
        // console.log(obj);
        this.nodeChanged.next(this.node);
    }
    sendData(obj: any) {
        this.setNode(obj);
        console.log('data sent\n\n');
        console.log(this.getNode());
        alert('Данные отправлены!\n\n'+JSON.stringify(this.getNode());
    }

    storeHandy(type:string, obj: Node) {
        switch (type.toUpperCase()) {
            case 'IP': {
                this.ipHandy = this.creator.getObject(type, obj);
                break;
            }
            case 'DNS': {
                this.dnsHandy = this.creator.getObject(type, obj);
                break;
            }
        }
    }

    restoreHandy(type:string) {
        switch (type.toUpperCase()) {
            case 'IP': {
                // console.log(this.node);
                this.setNode(this.ipHandy);
                // console.log(this.node);
                break;
            }
            case 'DNS': {
                this.setNode(this.dnsHandy);
                break;
            }
        }
    }

    setDhcp(type:string) {
        switch (type.toUpperCase()) {
            case 'IP': {
                this.setNode(ip);
                break;
            }
            case 'DNS': {
                this.setNode(dns);
                break;
            }
        }
    }

    nodeReset()
    {
        this.setNode(node);
    }


}