export class Node {
    public mac: string;
    public host:string;
    public ip:string;
    public mask:string;
    public gateway:string;
    public server1:string;
    public server2:string;

    constructor(name: string, host: string,ip: string, mask: string, gateway: string,server1:string,server2:string) {
        this.mac=name;
        this.host=host;
        this.ip=ip;
        this.mask=mask;
        this.gateway=gateway;
        this.server1=server1;
        this.server2=server2;
    }

}
export class NET {
    public mac: string;
    public host:string;

    constructor(name: string='', host: string='') {
        this.mac=name;
        this.host=host;
    }
}

export class IP {
    public ip:string;
    public mask:string;
    public gateway:string;

    constructor(ip: string = '', mask: string = '', gateway: string = '') {
        this.ip=ip;
        this.mask=mask;
        this.gateway=gateway;
    }
}

export class DNS {
    public server1:string;
    public server2:string;

    constructor(server1:string='',server2:string='') {
        this.server1=server1;
        this.server2=server2;
    }
}

export const  node: Node = {
    mac:'20:46:01:a1:20:75',
    host: 'VecowK5-2046a1012546',
    ip: '192.168.106.27',
    mask: '255.255.240.0',
    gateway: '192.168.11.254',
    server1: '192.168.11.254',
    server2: '8.8.8.8'
}

export const  net: NET = {
    mac:'20:46:01:a1:20:75',
    host: 'VecowK5-2046a1012546'
}
export const  ip: IP = {
    ip: '192.168.106.27',
    mask: '255.255.240.0',
    gateway: '192.168.11.254'
}
export const  dns: DNS = {
    server1: '192.168.11.254',
    server2: '8.8.8.8'
}