import { Injectable } from '@angular/core';
import { Factory } from './factory';
import { NET,IP,DNS, net,ip,dns } from '../models/node.model';

@Injectable()
export class Creator {

    constructor(private factory: Factory) {
    }
    getObject(type: string, obj: any) {

        switch (type.toUpperCase()) {
            case 'NET': {
                let rObj:NET = this.factory.create<NET>(NET);
                Object.assign(rObj,obj);
                return rObj;
            }
            case 'IP': {
                let rObj:IP = this.factory.create<IP>(IP);
                rObj = (({ip,mask,gateway})=>({ip,mask,gateway}))(obj);
                return rObj;
            }
            case 'DNS': {
                let rObj:IP = this.factory.create<IP>(IP);
                rObj = (({server1,server2})=>({server1,server2}))(obj);
                return rObj;
            }

        }
    }

}