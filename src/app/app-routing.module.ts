import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NetsComponent } from "./nets/nets.component";

const appRoutes: Routes = [
    { path: '', redirectTo: '/nets', pathMatch: 'full' },
    { path: 'nets', component: NetsComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
