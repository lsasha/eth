import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { Factory } from "./nets/lib/factory";
import { Creator } from "./nets/lib/creator";
import { NetsComponent } from './nets/nets.component';
import { NodeService} from "./nets/services/node.service"


@NgModule({
  declarations: [
    AppComponent,
    NetsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule

  ],
  providers: [
      NodeService,
      Factory,
      Creator
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
